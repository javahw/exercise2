package com.mainpackage;

public class Main {

    public static void main(String[] args) {
	    Warehouse warehouse = new Warehouse(1);
	    Thread dispatcher = new Thread(){
	        public void run(){
	        	while (true)
					warehouse.dispatch();
			}
        };
	    Thread supplier = new Thread(){
	        public void run(){
	        	while (true)
					warehouse.receive(10);
			}
        };
	    dispatcher.start();
	    supplier.start();
    }
}
