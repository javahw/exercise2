package com.mainpackage;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Warehouse {
    private ReentrantLock reentrantLock;
    private Condition condition;

    private Integer products;

    public Warehouse(int products){
        this.products = products;
        reentrantLock = new ReentrantLock();
        condition = reentrantLock.newCondition();
    }

    public void dispatch(){
        reentrantLock.lock();
        try{
            while (products <= 0)
                condition.await();
            products--;
            System.out.println("Warehouse has dispatched one product");
            condition.signal();
        }catch (InterruptedException e){
            reentrantLock.unlock();
        }
    }

    public void receive(int n){
        reentrantLock.lock();
        try{
            while (products >= 5)
                condition.await();
            products += n;
            System.out.println("Warehouse has received " + n + " products");
            condition.signal();
        }catch (InterruptedException e){
            reentrantLock.unlock();
        }
    }
}
